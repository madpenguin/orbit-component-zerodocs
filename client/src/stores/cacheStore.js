import { defineStore } from 'pinia'

export const useCacheStore = defineStore('cacheStore', {
    state: () => {
        return {
            model: 'cache',
            path_forward: new Map(),
            path_reverse: new Map(),
        }
    },
    actions: {
        hook_init (self) {
            this.create_lookup('by_path', this.path_forward, this.path_reverse, (d) => `${d.provider}|${d.project}|${d.branch}|${d.path}`.toLowerCase())
        },
        sort (params) {},
        get_api_remote (root, params, callback) {
            const socket = this._sockets.get(root)
            if (!socket) throw 'socket not initialised'
            socket.emit('get_api_remote', params.provider, params.project, params.branch, params.path, (response) => {
                // console.log("API>", response)
                if (response && response.ok) callback(response)
                else console.warn('call failed :: ', response)
            })
        },
        search (root, text, callback) {
            const socket = this._sockets.get(root)
            if (!socket) throw 'socket not initialised'
            socket.emit('search', text, (response) => {
                if (response && response.ok) callback(response)
                else console.warn('call failed :: ', response)
            })
        },
        // hook_invalidate (response) {
        //     response.data.forEach(id => {
        //         this.remove_tree(id)
        //     })
        // },
        // cache_put (root, to_create, to_delete, callback=null) {
        //     const socket = this._sockets.get(root)
        //     if (!socket) throw 'socket not initialised'
        //     socket.emit('cache_put', to_create, to_delete, (response) => {
        //         if (callback) callback(response)
        //     })
        // },
        get_project_id (root, params, callback=null) {
            // console.log("<<CACHESTORE FETCH>>")
            const socket = this._sockets.get(root)
            if (!socket) throw 'socket not initialised'
            socket.emit('get_project_id', params, (response) => {
                if (callback) callback(response)
            })
        },
        fetch (root, params, force, callback=null) {
            // console.log("CACHESTORE FETCH", root, params)
            const socket = this._sockets.get(root)
            if (!socket) throw 'socket not initialised'
            socket.emit('cache_fetch', params, force, (response) => {
                if (callback) callback(response)
                // this.query({
                //     root: root,
                //     model: this.model,
                //     label: 'watch',
                //     component: root,
                //     method: `api_${this.model}_get_ids`,
                //     ids: [response._id]
                // }, (response) => {
                //     if (!response || !response.ok) throw new Error(response.error)
                // })
            })
        },
        update (root, old_data, new_data) {
            const socket = this._sockets.get(root)
            if (!socket) throw 'socket not initialised'
            socket.emit('cache_put', {old_data: old_data, new_data: new_data}, (response) => {
                if (!response||!response.ok) this.error = response.error
            })
        },
        populate (root, chain, callback=null) {
            // console.log("<< CACHESTORE POPULATE >>")
            this.query({
                model: this.model,
                label: root,
                component: root,
                method: `api_${this.model}_get_ids`,
                filter: {root: root, isLeaf: 1, path: ''},
                chain: chain
            }, (response) => {
                // console.log("RESPONSE>", response)
                if (!response || !response.ok) throw new Error(response)
                if (callback) callback(response)
            })
        }
    },
    useOrmPlugin: true
})
