import { collapseTransitionRtl } from 'naive-ui/es/collapse-transition/styles'
import { defineStore } from 'pinia'

export const useProjectStore = defineStore('projectStore', {
    state: () => {
        return {
            model: 'projects',
            key_forward: new Map(),
            key_reverse: new Map(),
        }
    },
    actions: {
        hook_init (self) {
            this.create_lookup('key', this.key_forward, this.key_reverse, (d) => `${d.provider}|${d.project}|${d.branch}|${d.path}`.toLowerCase())
        },
        hook_sort (params) {
            this.local_ids.get(params.label).sort((a,b) => {
                const aa = this.data.get(a)
                const bb = this.data.get(b)
                if (aa.index < bb.index) return -1
                if (aa.index > bb.index) return 1
                return 0
            })
        },
        getProjectId (root, params, callback) {
            const socket = this._sockets.get(root)
            if (!socket) throw 'socket not initialised'
            console.log("Emitting proj req", socket, params)
            socket.emit('get_project_id', params, (response) => {
                console.log("Project response>", response)
                return callback(response)
            })
        },
        add (root, params, callback) {
            const socket = this._sockets.get(root)
            if (!socket) throw 'socket not initialised'
            socket.emit('get_project_id', params, (response) => {
                if (!response||!response.ok) return callback(response)
                params.project_id = response.id
                socket.emit('project_put', params, (response) => {
                    return callback(response)
                })    
            })
        },
        remove (root, params, callback=null) {
            const socket = this._sockets.get(root)
            if (!socket) throw 'socket not initialised'
            socket.emit('project_remove', params, (response) => {
                if (callback) callback(response)
            })
        },
        populate (root, chain, callback=null) {
            this.query({
                model: this.model,
                label: root,
                component: root,
                method: `api_${this.model}_get_ids`,
                filter: {root: root},
                chain: chain
            }, (response) => {
                if (!response || !response.ok) throw new Error(response ? response.error : `no query: ${method}`)
                if (callback) callback(response)
            })
        },
        renumber (root, ids) {
            const socket = this._sockets.get(root)
            socket.emit('renumber', {ids:ids}, (response) => {
                if (!response||!response.ok) throw new Error(response)
            })
        }
    },
    useOrmPlugin: true
})
