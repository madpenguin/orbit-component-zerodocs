import { h, reactive } from 'vue'
import { NIcon } from 'naive-ui'
import { GithubFilled, GitlabFilled } from '@vicons/antd';
import { LogoPython } from '@vicons/carbon';
import { FileMarkdownTwotone, FileUnknownTwotone, FolderTwotone, CodeTwotone, TagTwotone } from '@vicons/antd';

const difference = (a, b) => new Set([...a].filter(x => !b.has(x)))

class BaseTree {

    constructor (tree) {
        this.tree = tree
        this.index = new Map()
        this.key = '0'
        this.selected = reactive([])
        this.expanded = reactive([])
        this.checked = reactive([])
    }

    sort () {
        this.tree.sort((a,b) => {
            if (a.index < b.index) return -1
            if (a.index > b.index) return 1
            return 0
        })
    }

    find_recurse = (node, key)=> {
        if (node._id == key) return node
        return (node.children||[]).some((node) => {
            return this.find_recurse(node, key)
        })
    }

    find = (key) => {
        return this.tree.some((node) => {
            return this.find_recurse(node, key)
        })
    }

    // expandAllrecurse = (expanded_set, node) => {
    //     if (node._id) {
    //         expanded_set.add(node._id) 
    //     }
    //     if (node.children) {
    //         node.children.forEach((node) => {
    //             this.expandAllrecurse(expanded_set, node)
    //         })
    //     }
    // }
    // expandAll = () => {
    //     // FIXME: replace with tree "default-expand-all"
    //     let expanded_set = new Set()
    //     this.tree.forEach((node) => {
    //         this.expandAllrecurse(expanded_set, node)
    //     })
    //     expanded_set.forEach((key) => {
    //         this.expanded.push(key)
    //     })
    // }

    delete_tree_recurse (node) {
        let key, index = 0
        let children = node && node.item && node.item.children ? node.item.children : []
        children.forEach(node => {
            key = node && node.key ? node.key : null
            index = this.selected.indexOf(key)
            if (index != -1) this.selected.splice(index, 1)
            index = this.expanded.indexOf(key)
            if (index != -1) this.expanded.splice(index, 1)
            index = this.checked.indexOf(key)
            if (index != -1) this.checked.splice(index, 1)
            this.delete_tree_recurse(node)
        })
    }
    delete_tree = (key, node) => {
        if (!node) return
        this.delete_tree_recurse(node)
        if (node.parent) {
            node.parent.children.splice(0, node.parent.children.length, ...node.parent.children.filter(node => node.key != key))
            if (!node.parent.children.length) {
                node = this.index.get(key)
                if (node) this.delete_tree (node.key, node)
            }    
        } else {
            this.tree.splice(0, this.tree.length, ...this.tree.filter(node => node._id != key))
        }
    }
    update_new = (item) => {
        item = {...item}
        const i = this.tree.findIndex(node => node.root == item.root && node.provider == item.provider && node.project == item.project && node.branch == item.branch)
        if (i >= 0) {
            this.tree[i] = Object.assign(this.tree[i], item)
        } else {
            let cls, icon
            switch (item.provider) {
                case 'gitlab':  cls = 'ext-gitlab'; icon = GitlabFilled; break
                case 'github':  cls = 'ext-github'; icon = GithubFilled; break
            }
            item.prefix = () => h(NIcon, {class: cls}, {default: () => h(icon)})
            item.isLeaf = false
            item.key = item._id
            this.tree.push(item)
        }
        this.index.set(item.key, {item: item, parent: null})
    }
    update_root = (response) => {
        if (response.ids) {
            const new_ids = new Set(response.ids)
            const old_ids = new Set(response.context.local_ids.get(response.params.label))
            difference(old_ids,new_ids).forEach(id => this.delete_tree (id, this.index.get(id)))
            response.ids.forEach((id) => {
                this.update_new(response.context.data.get(id))
            })
        }
        if (response.data) response.data.forEach(item => this.update_new({...item}))
    }
}

class LocalTree extends BaseTree {

    insert_new = (item) => {
        const root = this.tree.find(node => node.root == item.root && node.provider == item.provider && node.project == item.project && node.branch == item.branch)
        this.insert_tree(item.path.split("/"), {...item}, root, [], true)
    }
    trim_branch = (node) => {
        if (!node.isLeaf) {
            if (!node.children || !node.children.length) {
                this.delete_tree (node.key, this.index.get(node._id))
            } else {
                node.children.forEach(node => {
                    this.trim_branch (node)
                })
            }
        }
    }
    trim_tree = () => {
        return
        this.tree.forEach(node => {
            this.trim_branch (node)
        })
    }
    update_branch = (response) => {
        if (response.ids) {
            const new_ids = new Set(response.ids)
            const old_ids = new Set(response.context.local_ids.get(response.params.label))
            difference(old_ids, new_ids).forEach(id => {
                this.delete_tree (id, this.index.get(id))
            })
            response.ids.forEach(id => {
                this.insert_new(response.context.data.get(id))
            })
            this.trim_tree()
        }
        if (response.data) {
            response.data.forEach(item => {
                this.insert_new(item)
            })
            this.trim_tree ()
        }
    }
    get = (id) => {
        return this.index.get(id)
    }
    get_selected = () => {
        return this.index.get(this.selected[0])
    }
    decorate_tree (tree, item, sort=false) {
        if (sort) tree.children.sort((a,b) => {
            if (a.isLeaf == b.isLeaf) return a.label > b.label ? 1 : -1
            return a.isLeaf < b.isLeaf ? 1 : -1
        })
        // if (this.open_on_insert) this.expand_tree(tree._id)
        if (item.children && item.children.length) {
            item.isLeaf = false
            item.children.forEach(node => {
                let icon = ''
                switch (node.type) {
                    case 'method':      icon = CodeTwotone; break
                    case 'property':    icon = TagTwotone; break
                    default:            icon = FileUnknownTwotone; break
                }
                node.prefix = () => h(NIcon, {class: node.type}, {default: () => h(icon)})
            })
        }
    }
    insert_tree = (path, item, tree, root='', sort=false) => {
        // console.log("Insert>", item.root, tree.root)
        let label = path.shift()
        root.push(label)
        parent = root.join('/')
        if(!tree) {
            console.log("Empty tree = no insert")
            console.log("Path=", path)
            console.log("Item=", item)
            return
        }
        if (!tree.children) tree.children = reactive([])
        
        if (!path.length) {
            if (typeof(tree) == 'undefined') {
                console.log("Empty children, abort!")
                return
            }
            let node = tree.children.find(n => { return n.path == parent})
            if (node) {
                node.label = item.label
                if (item.children && item.children.length) {
                    node.children = item.children
                    node.isLeaf = false
                    this.decorate_tree(tree, node, false)
                } else {
                    node.children = reactive([])
                    node.isLeaf = true
                }
            } else {
                tree.isLeaf = false
                if (item.label) {
                    if (item.label.toLowerCase() == 'readme.md') return
                    const ext = 'ext-' + item.path.split('.').pop()
                    let icon = FileUnknownTwotone
                    switch (ext) {
                        case 'ext-md':          icon = FileMarkdownTwotone; break;
                        case 'ext-py':          icon = LogoPython; break;
                        default:
                            if (!item.isLeaf) icon = FolderTwotone;
                    }
                    item.prefix = () => h(NIcon, {class: ext}, {default: () => h(icon)})
                    tree.children.push(item)
                    this.decorate_tree(tree, item, sort)
                    // console.warn("Goog label>", item)
                } else {
                    console.warn("Bad label>", item)
                }
            }
            this.index.set(item._id, {item: item, parent: tree})
            return
        }
        let node = tree.children.find(n => { return n.path == parent })
        if (!node) {
            node = {
                label: label,
                isLeaf: false,
                path: parent,
                _id: ++this.key,
                prefix: () => h(NIcon, {class: 'ext-folder'}, {default: () => h(FolderTwotone)}),
                provider: item.provider,
                project: item.project,
                root: item.root,
            }
            tree.children.push(node)
            tree.isLeaf = false
            this.index.set(node._id, {item: node, parent: tree})
        }
        if (!node.children) node.children = reactive([])
        this.insert_tree(path, item, node, root)
    }
    delete_tree = (key, node) => {
        if (!node) return
        if (node.parent && node.parent.children) {
            node.parent.children.splice(0, node.parent.children.length, ...node.parent.children.filter(node => node.key != key))
            if (!node.parent.children.length) {
                node = this.index.get(key)
                if (node) this.delete_tree (node.key, node)
            }    
        }
        else {
            this.tree.splice(0, this.tree.length, ...this.tree.filter(node => node._id != key))
        }
    }
}

class RemoteTree extends BaseTree {

    update_branch = (response) => {
        console.log(">> UPDATE REMOTE BRANCH >> ", this.tree, response)
    }
    set_checked = (keys) => {
        this.checked.splice(0, this.checked.length, ...keys)
    }
    get_selected = () => {
        if (this.selected.length) {
            return this.index.get(this.selected[0])
        }
        return null
    }
}

export {
    LocalTree,
    RemoteTree
}