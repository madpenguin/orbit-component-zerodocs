import ZeroDocs from '@/../node_modules/orbit-component-zerodocs';
import ApiOutlined from '@vicons/material/ApiOutlined.js'
import { shallowRef } from 'vue'

export function menu (app, router, menu) {
    const IconAPI = shallowRef(ApiOutlined)
    app.use(ZeroDocs, {
        router: router,
        menu: menu,
        root: 'api',
        buttons: [
            {name: 'zerodocs', text: 'API'  , component: ZeroDocs , icon: IconAPI , pri: 1, path: '/zerodocs', meta: {root: 'zd1', host: location.host}},
        ]
    })
}