# Orbit Component :: ZeroDocs

#### This repository contains an Orbit Framework component which provides inline documentation on-the-fly from a projects source repository. Currently only plublic Gitlab repositories are supported but private repo's and other providers are relatively easy to add.

More documentation to come ...