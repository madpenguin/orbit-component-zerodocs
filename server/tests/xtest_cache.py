from orbit_component_base.src.orbit_config import OrbitConfig
from orbit_component_base.src.orbit_database import OrbitDatabase
from orbit_component_base.src.orbit_shared import world
from orbit_database import Doc
from schema.Cache import CacheCollection, CacheTable
from asyncio import sleep
from loguru import logger as log
import pytest

DELAY = 0.1
PROVIDER = 'gitlab'
PROJECT = 'orbit-vcheck'
PROJECT_ID = 44869863
BRANCH = 'main'

@pytest.fixture
async def db():
    world.conf = OrbitConfig('unit_tests').open()
    db = OrbitDatabase().open()
    CacheCollection().open(db)
    CacheCollection().empty()
    yield db
    db.close()


@pytest.mark.asyncio
async def test_fetch (db):

    results = []

    async def monitor(docs):
        nonlocal results
        results += docs

    async for database in db:
        doc = Doc({
            'key': 'e88f62ded0558bb7e4372c155d8153f6b6faf927',
            'label': 'vcheck_config',
            'path': 'server/src/vcheck_config.py',
            'type': 'blob',
            'isLeaf': True,
            'provider': 'gitlab',
            'project': 'orbit-vcheck',
            'branch': 'main',
            'project_id': PROJECT_ID
        })
        CacheTable().norm_tb.append(doc)


        CacheTable().norm_tb.watch(callback=monitor)
        result = await CacheCollection().get_project_id(doc.doc)
        assert result['ok'] == True, 'get_project_id failed'
        assert result['id'] == PROJECT_ID
        assert len(results) == 1
        
        # a = await CacheCollection().fetch({'provider': PROVIDER, 'project_id': project_id, 'path': 'README.md'})
        # b = await CacheCollection().fetch({'provider': PROVIDER, 'project_id': project_id, 'path': 'README.md'})
        # assert a==b, 'live and cache not the same!'
        # await sleep(2)
        # # keys = []
        # lower = Doc({'path': '/'})
        # upper = Doc({'path': f'/{chr(255)}'})
        # for result in APIsCollection().filter(index_name='by_parent', lower=lower, upper=upper):
        #     keys.append(result.key)

        # assert keys == [b'|LICENSE.md', b'|README.md', b'|client', b'|screenshots', b'|server'], 'root error'
        # await sleep(DELAY)
        # assert len(results) == 28, 'audit failure'