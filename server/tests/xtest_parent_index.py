from orbit_component_base.src.orbit_config import OrbitConfig
from orbit_component_base.src.orbit_database import OrbitDatabase
from orbit_component_base.src.orbit_shared import world
from orbit_database import Doc
from schema.APIs import APIsCollection, APIsTable
from asyncio import sleep
import pytest

DELAY = 0.1


@pytest.fixture
async def db():

    collections = [APIsCollection]
    world.conf = OrbitConfig('unit_tests').open()
    db = OrbitDatabase(collections).open()
    APIsCollection().empty()
    yield db
    db.close()


@pytest.mark.asyncio
async def test_parent_index_root (db):

    results = []

    async def monitor(docs):
        nonlocal results
        results += docs

    async for database in db:
        APIsTable().norm_tb.watch(callback=monitor)
        APIsTable().norm_tb.import_from_file('tests/data/data1.json')

        keys = []
        lower = Doc({'path': '/'})
        upper = Doc({'path': f'/{chr(255)}'})
        for result in APIsCollection().filter(index_name='by_parent', lower=lower, upper=upper):
            keys.append(result.key)

        assert keys == [b'|LICENSE.md', b'|README.md', b'|client', b'|screenshots', b'|server'], 'root error'
        await sleep(DELAY)
        assert len(results) == 28, 'audit failure'


@pytest.mark.asyncio
async def test_parent_index_client (db):

    async def monitor(docs):
        nonlocal results
        results += docs

    async for database in db:
        APIsTable().norm_tb.watch(callback=monitor)
        await sleep(DELAY)
        APIsTable().norm_tb.import_from_file('tests/data/data1.json')
        results = []

        keys = []
        lower = Doc({'path': 'client/'})
        upper = Doc({'path': 'client/{chr(255)}'})
        for result in APIsCollection().filter(index_name='by_parent', lower=lower, upper=upper):
            keys.append(result.key)

        assert keys == [b'client|Makefile', b'client|README.md', b'client|index.html', b'client|package-lock.json', b'client|package.json', b'client|src', b'client|vite.config.js', b'client|vitest.config.js', b'client|vue.config.js'], 'client error'
        await sleep(DELAY)
        assert len(results) == 28, 'audit failure'


@pytest.mark.asyncio
async def test_parent_index_server (db):

    async def monitor(docs):
        nonlocal results
        results += docs

    async for database in db:
        APIsTable().norm_tb.watch(callback=monitor)
        await sleep(DELAY)
        APIsTable().norm_tb.import_from_file('tests/data/data1.json')
        results = []

        keys = []
        lower = Doc({'path': 'server/'})
        upper = Doc({'path': 'server/{chr(255)}'})
        for result in APIsCollection().filter(index_name='by_parent', lower=lower, upper=upper):
            keys.append(result.key)
        assert keys == [b'server|pyproject.toml', b'server|src', b'server|vcheck.spec']
        await sleep(DELAY)
        assert len(results) == 28, 'audit failure'
