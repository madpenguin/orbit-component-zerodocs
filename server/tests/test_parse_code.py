from collections import Counter
import tokenize
from io import StringIO

words = Counter()
with open('tests/data/table.py') as io:
    text = io.read()
    
with StringIO(text) as io:
    tokens = tokenize.generate_tokens(io.readline)
    for token in tokens:
        word = token.string.strip()
        if not word.startswith('"') and not word.startswith("'") and not word.startswith("#"):
            if len(word) > 1:
                words.update([word])
        for word in word.replace('"', '').replace("'", '').replace("\n", " ").replace('#', '').split(" "):
            if len(word) > 1 and word[0] >= '0' and word[0] <= 'z':
                words.update([word])
 
print(words)    



# def handle_node (node):
#     if isinstance (node, ast.ImportFrom):
#         words.update([node.module])
#     elif isinstance (node, ast.Import):
#         for item in node.names:
#             handle_node(item)
#         # words.update([node.module])
#     elif isinstance(node, ast.Load):
#             pass
#     elif isinstance(node, ast.Expr):
#         handle_node (node.value)
#     elif isinstance(node, ast.Attribute):
#         # print ("1>>>", node.value, dir(node))
#         handle_node(node.value)
#     elif isinstance(node, ast.Constant):
#         print ("2>>>", node.value)
#     elif isinstance(node, ast.Name):
#         print ("3>>>", node.id)
#     elif isinstance(node, ast.alias):
#         print ("4>>>", node.asname)
#     elif isinstance(node, ast.ExceptHandler):
#         print ("5>>>", node.name)
#     elif isinstance(node, ast.Try):
#         pass
#     elif isinstance(node, ast.Module):
#         pass
#     elif isinstance(node, ast.Store):
#         pass
#     elif isinstance(node, ast.arguments):
#         pass
#     elif isinstance(node, ast.Return):
#         pass
#     elif isinstance(node, ast.Call):
#         if hasattr(node.func, "id"):
#             print ("61>>>", node.func.id)
#         elif hasattr(node.func, "value"):
#             handle_node(node.func.value)
#         else:
#             print("ERR")
#     elif isinstance(node, ast.keyword):
#         # print ("7>>>", node.value)
#         handle_node(node.value)
#     elif isinstance(node, ast.Assign):
#         print ("8>>>", node.type_comment)
#     elif isinstance(node, ast.ClassDef):
#         print ("A>>>", node.name)
#     elif isinstance(node, ast.Subscript):
#         print ("B>>>", node.slice)
#     elif isinstance(node, ast.FunctionDef):
#         print ("C>>>", node.name, node.type_comment)

#     else:
#         print(node, dir(node))
#     # if hasattr(node, 'id'):
#     #     print(node.id)
#     # elif hasattr(node, 'value'):
#     #     print(node.value)
#     # else:
#     #     print(node, dir(node), str(node))#


# for node in ast.walk (ast.parse(code)):
#     handle_node (node)
# print("Words>", words)