from orbit_component_base.src.orbit_config import OrbitConfig
from orbit_component_base.src.orbit_database import OrbitDatabase
from orbit_component_base.src.orbit_shared import world
from orbit_database import Doc
from orbit_component_zerodocs.schema.Cache import CacheCollection, CacheTable
from asyncio import sleep
from loguru import logger as log
import pytest

from gitlab import Gitlab
gitlab = Gitlab()



DELAY = 0.1
PROVIDER = 'gitlab'
PROJECT = 'orbit-vcheck'
PROJECT_ID = 44869863
BRANCH = 'main'

@pytest.fixture
async def db():
    world.conf = OrbitConfig('unit_tests').open()
    db = OrbitDatabase().open()
    CacheCollection().open(db)
    CacheCollection().empty()
    yield db
    db.close()


@pytest.mark.asyncio
async def test_fetch (db):

    result = await CacheCollection().get_project_id({
        'project': 'orbit-database',
        'provider': 'gitlab'
        })
    assert result.get('id') == 44968356, 'wrong project ID'
